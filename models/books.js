const Mongosee = require("./db") 

const BooksModel = Mongosee.model("books", {
    _id : String,
    title: {
        type : String,
        minlength: [5, 'Karakter Harus Lebih dari 5'],
    },
    author: String,
    publisher_date: {
        type : Date,
        dafault: Date.now,
    },
    pages: Number,
    language: String,
    publisher_id: String,
});
exports.Model = BooksModel
