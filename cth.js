 app.get("/register/",
        [
            check("name")
            .isEmail()
            .withMessage("Harus Email yah"),

            check("password")
            .isLength({ min: 5 })
            .withMessage("Harus 5 Karakter"),
        ],
        (req, res) => {
            // Finds the validation errors in this request and wraps them in an object with handy functions
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                //res.send(`Error name harus email ya dan password harus lebih dari 5 karakter`)
                return res.status(422).json({ errors: errors.array() });
            } else 
            res.json(req.body);
            /*res.send(`
            
            Helo email anda : ${req.params.name } 
            password : ${req.params.password}
            
            `)*/
        });
