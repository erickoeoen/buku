const Express = require("express")
var routes = Express.Router()
const books = require('../models/books');

routes.post("/", async (request, response) => {
        try {
            var book = new books.Model(request.body);
          
            var result = await book.save();
            
            response.send(result);
        } catch (error) {
            response.status(500).send(error);
        }
    });

routes.get("/", async (request, response) => {
    try {
        var result = await books.Model.find().exec();
        response.send(result);
    } catch (error) {
        response.status(500).send(error);
    }
});

routes.get("/:id", async (request, response) => {
    try {
        var book = await books.Model.findById(request.params.id).exec();
        response.send(book);
    } catch (error) {
        response.status(500).send(error);
    }
});

routes.put("/:id", async (request, response) => {
    try {
        var book = await books.Model.findById(request.params.id).exec();
        book.set(request.body);
        var result = await book.save();
        response.send(result);
    } catch (error) {
        response.status(500).send(error);
    }
});

routes.delete("/:id", async (request, response) => {
    try {
        var result = await books.Model.deleteOne({ _id: request.params.id }).exec();
        response.send(result);
    } catch (error) {
        response.status(500).send(error);
    }
});


module.exports = routes;