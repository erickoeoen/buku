const Express = require("express");
const BodyParser = require("body-parser");
const booksRouter = require('./routes/books')

const Mongoose = require("mongoose");

Mongoose.connect("mongodb://localhost:27017/express_mongoose");

//const Mongosee = require("./models/db") 

var app = Express();

app.use(BodyParser.json());
app.use(BodyParser.urlencoded({ extended: true }));
app.use('/books', booksRouter)

app.use(function (req, res, next) {
    return res.status(404).send({
        status: 404,
        massage: "Not Found"
    });
    })

app.use(function (req, res, next) {
        return res.status(500).send()
    })

app.listen(3000, () => {
    console.log("Listening at :3000...");
});

